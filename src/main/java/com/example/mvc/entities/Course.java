package com.example.mvc.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String courseName;

    @ManyToMany(mappedBy = "courses")
    private Set<Trainer> trainers;

}
