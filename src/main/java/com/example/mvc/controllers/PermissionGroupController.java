package com.example.mvc.controllers;


import com.example.mvc.entities.PermissionGroup;
import com.example.mvc.entities.User;
import com.example.mvc.services.PermissionGroupService;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

/*
Implemented CONSTRUCTOR injection using lombok library
 */

@Controller
@RequiredArgsConstructor
public class PermissionGroupController {

    private final PermissionGroupService permissionGroupService;

    @GetMapping("/groups")
    public String showUpdateForm(Model model) {
        List<PermissionGroup> permissionGroups = permissionGroupService.findAll();
        model.addAttribute("permissionGroups", permissionGroups);
        model.addAttribute("ourValue", "SDA JavaRigaRus2 Group; Groups page");
        return "permissionGroup-index";
    }

    @GetMapping("/groups/signup")
    public String showSignUpForm(PermissionGroup permissionGroup) {
        return "permissionGroup-add";
    }

    @PostMapping("/groups/add")
    public String addUser(@Valid PermissionGroup permissionGroup, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "permissionGroup-add";
        }

        permissionGroupService.save(permissionGroup);
        model.addAttribute("permissionGroups", permissionGroupService.findAll());
        return "permissionGroup-index";
    }

    @GetMapping("/groups/edit/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
        PermissionGroup permissionGroup = permissionGroupService.findById(id);
        model.addAttribute("permissionGroup", permissionGroup);
        return "permissionGroup-edit";
    }

    @PostMapping("/groups/update/{id}")
    public String updatePermissionGroup(@PathVariable("id") long id, @Valid PermissionGroup permissionGroup, BindingResult result, Model model) {
        if (result.hasErrors()) {
            permissionGroup.setId(id);
            return "permissionGroup-edit";
        }

        permissionGroupService.save(permissionGroup);
        model.addAttribute("permissionGroups", permissionGroupService.findAll());
        return "index";
    }

    @GetMapping("/groups/delete/{id}")
    public String deleteUser(@PathVariable("id") long id, Model model) {
        PermissionGroup permissionGroup = permissionGroupService.findById(id);
        permissionGroupService.delete(permissionGroup);
        model.addAttribute("permissionGroups", permissionGroupService.findAll());
        return "index";
    }
}
