package com.example.mvc.controllers;


import com.example.mvc.entities.User;
import com.example.mvc.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class UserController {

    //FIELD INJECTION
    @Autowired
    private UserService userService;

/*
CONSTRUCTOR INJECTION

    private final UserRepository userRepository;
    private final GroupRepository groupRepository;

    public UserController(final UserRepository repository, GroupRepository groupRepository) {
        this.userRepository = repository;
        this.groupRepository = groupRepository;
    }
*/
    @GetMapping("/")
    public String showUpdateForm(Model model) {
        List<User> users = userService.findAllUsers();
        model.addAttribute("users1", users);
        model.addAttribute("ourValue", "SDA JavaRigaRus2 Group");
        return "sec/index";
    }

    @GetMapping("/users/signup")
    public String showSignUpForm(User user) {
        return "user-add";
    }

    @PostMapping("/users/adduser")
    public String addUser(@Valid User user, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "user-add";
        }

        userService.save(user);
        model.addAttribute("users", userService.findAllUsers());
        return "index";
    }

    @GetMapping("/users/edit/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
        User user = userService.findById(id);
        model.addAttribute("user", user);
        return "user-edit";
    }

    @PostMapping("/users/update/{id}")
    public String updateUser(@PathVariable("id") long id, @Valid User user, BindingResult result, Model model) {
        if (result.hasErrors()) {
            user.setId(id);
            return "user-edit";
        }

        userService.save(user);
        model.addAttribute("users", userService.findAllUsers());
        return "index";
    }

    @GetMapping("/users/delete/{id}")
    public String deleteUser(@PathVariable("id") long id, Model model) {
        User user = userService.findById(id);
        userService.delete(user);
        model.addAttribute("users", userService.findAllUsers());
        return "index";
    }
}
