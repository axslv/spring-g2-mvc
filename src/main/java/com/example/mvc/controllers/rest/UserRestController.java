package com.example.mvc.controllers.rest;


import com.example.mvc.entities.User;
import com.example.mvc.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/rest")
public class UserRestController {

    private final UserService userService;

    @GetMapping
    public ResponseEntity<List<User>> findAll() {
        List<User> users = userService.findAllUsers();
        return ResponseEntity.ok(users); //JSON
    }

    @GetMapping("/users/byName/{name}") // http://localhost:8080/rest/users/byName/John
    public ResponseEntity<List<User>> findByName(@PathVariable String name) {
        List<User> users = userService.findByName(name);
        return ResponseEntity.ok(users);
    }
}
