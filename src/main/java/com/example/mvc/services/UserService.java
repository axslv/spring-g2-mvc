package com.example.mvc.services;

import com.example.mvc.entities.Role;
import com.example.mvc.entities.User;
import com.example.mvc.entities.web.UserRegistrationDto;
import com.example.mvc.repositories.PermissionGroupRepository;
import com.example.mvc.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private PermissionGroupRepository permissionGroupRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    public List<User> findByLogin(String login) {
        return userRepository.findByLogin(login);
    }

    public User save(UserRegistrationDto registration) {
        User user = new User();
        user.setLogin(registration.getLogin());
        user.setEmail(registration.getEmail());
        user.setPassword(passwordEncoder.encode(registration.getPassword()));
        user.setRoles(Arrays.asList(new Role("ROLE_USER")));
        return userRepository.save(user);
    }

    public User save(User id) {
        User users = userRepository.save(id);
        return users;
    }


    public Pageable findUsersPaged() {
        Pageable a = PageRequest.of(0, 5);
        return a;
    }

    public List<User> findAllUsers() {
        List<User> users = userRepository.findAll();
        return users;
    }

    public List<User> findByName(String name) {
        List<User> users = userRepository.findByName(name);
        return users;
    }

    public User findById(Long id) {
       User users = userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        return users;
    }

    public void delete(User id) {
        userRepository.delete(id);
    }

    //-----------------------------Spring security stuff------------------------------------------------

    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        List<User> user = userRepository.findByLogin(login);
        if (user.isEmpty()) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(user.get(0).getLogin(),
                user.get(0).getPassword(),
                mapRolesToAuthorities(user.get(0).getRoles()));
    }

    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles) {
        return roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());
    }


}
