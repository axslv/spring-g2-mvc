package com.example.mvc.services;

import com.example.mvc.entities.PermissionGroup;
import com.example.mvc.repositories.PermissionGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PermissionGroupService {

    @Autowired
    private PermissionGroupRepository permissionGroupRepository;

    public List<PermissionGroup> findAll() {
        List<PermissionGroup> users = permissionGroupRepository.findAll();
        return users;
    }

    public PermissionGroup findById(Long id) {
        PermissionGroup users = permissionGroupRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid permissionGroup Id:" + id));
        return users;
    }

    public PermissionGroup save(PermissionGroup id) {
        PermissionGroup users = permissionGroupRepository.save(id);
        return users;
    }

    public void delete(PermissionGroup id) {
        permissionGroupRepository.delete(id);
    }


}
